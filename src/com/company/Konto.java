package com.company;

public class Konto {
	private String userName;
	private String password;

	private boolean isKlient;

	
	public Konto(String userName, String password, boolean isKlient) {
		this.userName = userName;
		this.password = password;
		this.isKlient = isKlient;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		if (password.equals("password")) return;
		
		this.password = password;
	}

	public boolean getIsKlient() {
		return isKlient;
	}
	
}
