package com.company;

import java.util.Scanner;

public class Logowanie {
	Konto[] baza = new Konto[2];

	private Konto konto;

	public Konto getKonto() {
		return konto;
	}

	public Logowanie() {
		baza[0] = new Konto("user1", "password", true);
		baza[1] = new Konto("user2", "password2", false);

	}

	public boolean login() {
		Scanner scanner = new Scanner(System.in);
		String userName;
		String password;

		System.out.println("Podaj login:");
		userName = scanner.nextLine();

		System.out.println("Podaj has�o:");
		password = scanner.nextLine();

		for (int i = 0; i < baza.length; i++) {
			if (userName.equals(baza[i].getUserName())) {
				if (password.equals(baza[i].getPassword())) {
					this.konto = baza[i];
					return true;
				}
			}
		}

		return false;
	}

}
