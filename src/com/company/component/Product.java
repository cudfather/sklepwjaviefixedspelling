package com.company.component;

import com.company.Konto;

public class Product {

    private Konto konto;

    public Product(Konto konto) {
        this.konto = konto;
    }

    public void create() {

        if (!konto.getIsKlient())
            System.out.println("Tworzenie produktu.");
    }

    public void read() {

        System.out.println("Czytanie produktu.");
    }

    public void update() {
        if (!konto.getIsKlient())
            System.out.println("Update produktu.");
    }

    public void delete() {
        if (!konto.getIsKlient())
            System.out.println("Kasowanie produktu.");
    }

}
