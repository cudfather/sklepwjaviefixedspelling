package com.company;

import java.util.Scanner;

public abstract class Menu {

    abstract String[] getOpcje();
    abstract void executeComponent(int id);

    public void print() {
        String[] opcje = getOpcje();
        Scanner scanner  = new Scanner(System.in);
        for (int i = 0; i < opcje.length; i++) {
            System.out.format("[%s]: %s\n", i + 1, opcje[i]);
        }
        String wybor;
        wybor = scanner.nextLine();

        executeComponent(wybor.charAt(0));

    }

}
